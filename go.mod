module gitlab.com/raspi-alpine/go-raspi-alpine

go 1.17

require (
	github.com/GehirnInc/crypt v0.0.0-20190301055215-6c0105aabd46
	github.com/stretchr/testify v1.4.0
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
