# Go system interface library

[![godoc](https://godoc.org/gitlab.com/raspi-alpine/go-raspi-alpine?status.svg)](https://godoc.org/gitlab.com/raspi-alpine/go-raspi-alpine)

This library provides an interface to systems build with the
[Raspberry PI Alpine Linux Image Builder](https://gitlab.com/raspi-alpine/builder).

It provides the following features:
  * Change System Settings:
    * Root Password
    * Network
    * Enable/Disable SSH server
    * Set Time Zone
  * Reboot & Shutdown
  * Reset of UBoot boot counter
  * Install of update image

